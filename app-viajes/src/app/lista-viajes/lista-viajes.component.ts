import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {DestinoViaje} from "./../models/modelo-viaje.model";
import { DestinosApiClient } from './../models/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../models/destinos-viajes-state-model';


@Component({
  selector: 'app-lista-viajes',
  templateUrl: './lista-viajes.component.html',
  styleUrls: ['./lista-viajes.component.css']
})
export class ListaViajesComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates = string[];
  all;
  constructor(private destinosApiClient: DestinosApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito)
    .subscribe(d => {
      const fav = data;
      this.destinosApiClient.subscribeOnChange((d: DestinoViaje) => {
        if (d != null) {
          this.updates.push("se ha elegido a " + d.nombre);
        }
      });
    });
    store.select(state => state.destinos.items).subscribe(items => this.all = items);

  }

  ngOnInit(): void {
  }

  agregado(d: DestinoViaje) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(e: DestinoViaje){
    this.destinosApiClient.elegir(e);;
  }
  getAll()  {

  }
}
