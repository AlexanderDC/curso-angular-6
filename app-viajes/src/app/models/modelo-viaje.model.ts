import { ThrowStmt } from '@angular/compiler';
import { ObjectUnsubscribedError } from 'rxjs';

export class DestinoViaje{
    private selected: boolean;
    public servicios: string[];
    id = uuid();
    constructor(public nombre:string, public imageUrl:string, public votes: number = 0){
        this.servicios = ['pileta', 'desayuno'];
    }
    isSelected(): boolean{
        return this.selected;
    }
    setSelected(s: boolean){
        this.selected = s;
    }
    voteUp() {
        this.votes++;
    }
    voteDown() {
        this.votes--;
    }
}